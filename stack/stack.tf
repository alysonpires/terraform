
resource "aws_opsworks_stack" "infra" {
  	name = "${var.stack_name}"
  	region = "${var.aws_regiao}"
  	vpc_id = "${aws_vpc.vpc-stack.id}"
  	service_role_arn = "${var.service_role_arn}"
  	default_instance_profile_arn = "${var.default_instance_profile_arn}"
  	default_subnet_id = "${aws_subnet.snet-stack-a.id}"
  	default_os = "${var.ami}"
  # default_ssh_key_name = "nginx"
  # default_availability_zone = "${var.zona_b}"
  	configuration_manager_version = "12"
  	default_root_device_type = "ebs"
  	use_custom_cookbooks = "true"
  	agent_version = "LATEST"
  	use_opsworks_security_groups = false
	color = "${var.vermelho}"
  	custom_cookbooks_source = {
    	type = "git",
    	url = "git@bitbucket.org:bemobidev/opsworks_oi_plataformas.git"
    	#ssh_key = "~/.ssh/id_rsa"
    	#revision = "master"

		}

    tags {
      Environment = "Prod"
      Team = "${var.team_name}"
      Project = "${var.stack_name}"
    }

}
