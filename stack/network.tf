resource "aws_vpc" "vpc-stack" {
cidr_block = "${var.vpc_cidr}"
instance_tenancy = "default"
enable_dns_hostnames = "true"
enable_classiclink_dns_support = "true"
#enable_classiclink = "true"

/*
Saiba mais sobre o ClassicLink: https://docs.aws.amazon.com/pt_br/AWSEC2/latest/UserGuide/vpc-classiclink.html
*/

    lifecycle {
        create_before_destroy = true
    }

	tags {
		Name = "vpc-${var.stack_name}"
		Project = "${var.stack_name}"
		Team = "${var.team_name}"
		Environment = "Prod"

	}
}

resource "aws_internet_gateway" "igw-stack" {
vpc_id = "${aws_vpc.vpc-stack.id}"

    lifecycle {
        create_before_destroy = true
    }

	tags {
		Name = "igw-${var.stack_name}",
		Project = "${var.stack_name}",
		Team = "${var.team_name}"
		Environment = "Prod"
	}	
}

resource "aws_route_table" "rt-stack" {
	vpc_id = "${aws_vpc.vpc-stack.id}"
	route {
		cidr_block = "${var.rt_cidr}"
		gateway_id = "${aws_internet_gateway.igw-stack.id}"
	}	

    lifecycle {
        create_before_destroy = true
    }

	tags {
		Name = "rt-${var.stack_name}"
		Project = "${var.stack_name}"
		Team = "${var.team_name}"
		Environment = "Prod"
	}
}

resource "aws_main_route_table_association" "assoc_table" {
	vpc_id = "${aws_vpc.vpc-stack.id}"
	route_table_id = "${aws_route_table.rt-stack.id}"

    lifecycle {
        create_before_destroy = true
    }

}

resource "aws_subnet" "snet-stack-a" {
	vpc_id = "${aws_vpc.vpc-stack.id}"
	cidr_block = "${var.snet_cidr_a}"
	availability_zone = "${var.zona_a}"
	map_public_ip_on_launch = "true"

    lifecycle {
        create_before_destroy = true
    }

	tags {
		Name = "subnet-${var.stack_name}-a"
		Project = "${var.stack_name}"
		Team = "${var.team_name}"
		Environment = "Prod"
	}	
}

resource "aws_subnet" "snet-stack-b" {
	vpc_id = "${aws_vpc.vpc-stack.id}"
	cidr_block = "${var.snet_cidr_b}"
	availability_zone = "${var.zona_b}"
	map_public_ip_on_launch = "true"

    lifecycle {
        create_before_destroy = true
    }

	tags {
		Name = "subnet-${var.stack_name}-b"
		Project = "${var.stack_name}"
		Team = "${var.team_name}"
		Environment = "Prod"
	}	
}

resource "aws_subnet" "snet-stack-c" {
	vpc_id = "${aws_vpc.vpc-stack.id}"
	cidr_block = "${var.snet_cidr_c}"
	availability_zone = "${var.zona_c}"
	map_public_ip_on_launch = "true"

    lifecycle {
        create_before_destroy = true
    }

	tags {
		Name = "subnet-${var.stack_name}-c"
		Project = "${var.stack_name}"
		Team = "${var.team_name}"
		Environment = "Prod"
	}	
}

resource "aws_route_table_association" "zona_a" {
	subnet_id = "${aws_subnet.snet-stack-a.id}"
	route_table_id = "${aws_route_table.rt-stack.id}"

    lifecycle {
        create_before_destroy = true
    }

}

resource "aws_route_table_association" "zona_b" {
	subnet_id = "${aws_subnet.snet-stack-b.id}"
	route_table_id = "${aws_route_table.rt-stack.id}"

    lifecycle {
        create_before_destroy = true
    }

}

resource "aws_route_table_association" "zona_c" {
	subnet_id = "${aws_subnet.snet-stack-c.id}"
	route_table_id = "${aws_route_table.rt-stack.id}"

    lifecycle {
        create_before_destroy = true
    }

}

resource "aws_elb" "elb-stack-layer" {
	name = "elb-${var.stack_name}-${var.layer_name}"
	security_groups = ["${aws_security_group.sg_stack-layer.id}", "${aws_security_group.sg_stack-layer-elb.id}"] 
	subnets = ["${aws_subnet.snet-stack-a.id}", "${aws_subnet.snet-stack-b.id}", "${aws_subnet.snet-stack-c.id}"]
	#availability_zones = ["${var.zona_c}"]
	#internal = "true"

#  access_logs {
#    bucket = "infra"
#    interval = 60
#  }

listener {
	instance_port = 22
	instance_protocol = "tcp"
	lb_port = 80
	lb_protocol = "tcp"
}

health_check {
	healthy_threshold = 3
	unhealthy_threshold = 2
	timeout = 5
	target = "TCP:22"
	interval = 10
}

cross_zone_load_balancing = true
idle_timeout = 300
connection_draining = true
connection_draining_timeout = 300

    lifecycle {
        create_before_destroy = true
    }

tags {
	Name = "elb-${var.stack_name}-${var.layer_name}"
	Project = "${var.stack_name}"
	Team = "${var.team_name}"
	Environment = "Prod"
  }   
}
