# Variaveis para REGIÃO

variable "aws_regiao" {
  description = "Region for the VPC"
  default = "us-east-1"
}
 
###############################################################################

# Variáveis para VPC
variable "vpc_cidr" {
  description = "CIDR da VPC"
  default = "10.100.39.0/24"
}

variable "vpc_name"{
  description = "VPC da stack ${var.stack_name}"
  default = "vpc"
}

variable "vpc_id" {
  default = "xxx"
}

###############################################################################

# Variáveis para subnets
variable "snet_cidr_a" {
  description = "CIDR da subnet - ${var.stack_name}"
  default = "10.100.39.0/26"
}
 
variable "snet_cidr_b" {
  description = "CIDR da subnet - ${var.stack_name}"
  default = "10.100.39.64/26"
}

variable "snet_cidr_c" {
  description = "CIDR da subnet - ${var.stack_name}"
  default = "10.100.39.128/26"
}

variable "snet_cidr_d" {
  description = "CIDR da subnet - ${var.stack_name}"
  default = "10.100.39.192/26"
}

variable "snet_cidr_e" {
  description = "CIDR da subnet - ${var.stack_name}"
  default = "10.100.5.128/26"
}

variable "snet_cidr_f" {
  description = "CIDR da subnet - ${var.stack_name}"
  default = "10.100.6.128/26"
}

variable "snet_id_a" {
  default = "xxx"
}

variable "snet_id_b" {
  default = "xxx"
}

variable "snet_id_c" {
  default = "xxx"
}

variable "snet_id_d" {
  default = "xxx"
}

variable "snet_id_e" {
  default = "xxx"
}

variable "snet_id_f" {
  default = "xxx"
}

###############################################################################

# Variáveis para Route table
variable "rt_cidr" {
  description = "Route table - ${var.stack_name}"
  default = "0.0.0.0/0"
}

variable "rt" {
  description = "rt-${var.stack_name}"
  default = "xxx"
}

variable "rt_nat" {
  description = "rt-${var.stack_name}-nat"
  default = "xxx"
}

variable "rt_id" {
	default = "xxx"
}

###############################################################################

# Variáveis para instâncias 
variable "ami" {
  description = "Amazon Linux AMI"
  default = "Amazon Linux 2017.09"
}
 
variable "key_path" {
  description = "SSH Public Key path"
  default = "/home/core/.ssh/id_rsa.pub"
}

variable "instance_type" {
	default = "t2.micro"
}

###############################################################################

# Variáveis para Zonas de disponibilidade
variable "zona_a" {
  default = "us-east-1a"
}

variable "zona_b" {
  default = "us-east-1b"
}

variable "zona_c" {
  default = "us-east-1c"
}

variable "zona_d" {
  default = "us-east-1d"
}

variable "zona_e" {
  default = "us-east-1e"
}


variable "zona_f" {
  default = "us-east-1f"
}

###############################################################################

# Variaveis para security group
variable "sg-layer" {
  description = "Security Group da ${var.layer_name} - Stack ${var.stack_name}"
  default = "201.76.168.107/32"
}

variable "sg_elb" {
  description = "Security Group do elb - ${var.layer_name} - Stack ${var.stack_name}"
  default = "0.0.0.0/0"
}

###############################################################################

# Variavel para internet gateway
variable "igw_name" {
  description = "Internet Gateway"
default = "igw"
}

variable "igw_id" {
  description = "Internet Gateway"
  default = "xxx"
}

###############################################################################

# Variáveis para stack e layer

variable "stack_name" {
  default = "infra"
}
variable "stack_id" {
  default = "e21b7d94-6239-4d77-8d6f-048b88e97d32"
}

variable "layer_name" {
  default = "devops"
}

variable "deploy_default" {
  default = "deploy::default"
}

variable "team_name" {
  default = "Infraestrutura"
}

variable "app_type" {
  default = "java"
}

variable "service_role_arn" {
  default = "arn:aws:iam::842984071457:role/aws-opsworks-service-role"
}

variable "default_instance_profile_arn" {
  default = "arn:aws:iam::842984071457:instance-profile/aws-opsworks-ec2-role"
}

variable "path" {
  default = "plataformas-bemobi"
}

###############################################################################

# Variáveis para RDS

variable "dbuser" {
	default = "root"
}

variable "dbpassword" {
	default = "123qweBemobi"
}

variable "instance_class_rds" {
	default = "db.t2.micro"
}

variable "engine" {
	default = "mysql"
}

variable "engine_version_rds" {
	default = "5.7.21"
}

variable "mydb" {
	default = "mydb"
}

variable "public_accesible" {
	default = "true"
}

variable "backup_rds" {
	default = "04:00-05:00"
}

variable "janela_manutencao" {
	default = "sat:03:00-sat:04:00"
}

variable "role_cloudwatch" {
	default = "arn:aws:iam::aws:policy/service-role/AmazonRDSEnhancedMonitoringRole"
}

variable "family" {
	default = "mysql5.7"
}

###############################################################################

# Variáveis para elasticache - Redis/Memcached

variable "engine_redis" {
  description = "redis-${var.stack_name}"
	default = "redis"
}

variable "engine_version_redis" {
  description = "Versao do Redis"
	default = "3.2.10"
}

variable "node_type_redis" {
	default = "cache.m3.medium"
}

variable "cache_type" {
  default = "cache.m3.medium"
}

variable "parameter_group_name" {
	default = "default.redis3.2"
}

variable "snapshot_window" {
  default = "05:00-09:00"
}

variable "port_redis" {
	default = "6379"
}

variable "notification_topic_arn" {
	default = "arn:aws:sns:us-east-1:012345678999:my_sns_topic"
}

###############################################################################

# Teste

variable "tags" {
  default = {
	"Name" = "Teste"
	"Project" = "Agora vai"
	"Team" = "Sera"
	"Environment" = "Prod"
  }
}

###############################################################################

# Habilitando cor na caixa da stack

variable "azul" {
	default = "rgb(38, 146, 168)"
}

variable "roxo" {
	default = "rgb(135, 61, 98)"
}

variable "roxo_escuro" {
	default = "rgb(111, 86, 163)"
}

variable "azul_escuro" {
	default = "rgb(45, 114, 184)"
}

variable "verde" {
	default = "rgb(57, 131, 94)"
}

variable "verde_abacate" {
	default = "rgb(100, 131, 57)"
}

variable "amarelo" {
	default = "rgb(184, 133, 46)"
}

variable "laranja" {
	default = "rgb(209, 105, 41)"
}

variable "vermelho" {
	default = "rgb(186, 65, 50)"
}
