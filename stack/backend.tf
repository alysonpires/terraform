terraform {
  backend "s3" {
    bucket         = "terraform-statebackup"
    key            = "terraform.tfstate"
    region         = "us-east-1"
    encrypt        = false
    #dynamodb_table = "terraform-locks"
  }
}
