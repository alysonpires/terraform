resource "aws_opsworks_custom_layer" "layer" {
    name = "${var.layer_name}"
    short_name = "${var.layer_name}"
    stack_id = "${var.stack_id}"
    auto_healing = "true"
    instance_shutdown_timeout = "120"
    custom_setup_recipes = ["${var.layer_name}::default", "zabbix-agent::default",]
#   custom_configure_recipes = ["logrotate::default",]
    custom_deploy_recipes = ["deploy::default"]
    custom_shutdown_recipes = ["${var.layer_name}::revoke",]
    auto_assign_public_ips = true
#   auto_assign_elastic_ips = true
    custom_security_group_ids = ["${aws_security_group.sg_stack-layer.id}", "${aws_default_security_group.default.id}"]
    elastic_load_balancer = "${aws_elb.elb-stack-layer.id}"
#   drain_elb_on_shutdown = "false"
    use_ebs_optimized_instances = "true"

		}

/* 
Montando uma partição adicional nas instâncias do opsworks
	ebs_volume = {
    	mount_point = "/mnt/data"

#Issue: o valor "tamanho" não é atualizado quando alterado
	size            = 40
    number_of_disks = 1
    raid_level      = "None"
    type            = "gp2"
}

*/
