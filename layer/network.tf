/*

resource "aws_vpc" "vpc-stack" {
cidr_block = "${var.vpc_cidr}"
instance_tenancy = "default"
enable_dns_hostnames = "true"
enable_classiclink_dns_support = "true"
#enable_classiclink = "true"

Saiba mais sobre o ClassicLink: https://docs.aws.amazon.com/pt_br/AWSEC2/latest/UserGuide/vpc-classiclink.html

    lifecycle {
        create_before_destroy = true
    }

	tags {
		Name = "vpc-${var.stack_name}"
		Project = "${var.stack_name}"
		Team = "${var.team_name}"
		Environment = "Prod"

	}
}




resource "aws_internet_gateway" "igw-stack" {
vpc_id = "${var.vpc_id}"

    lifecycle {
        create_before_destroy = true
    }

	tags {
		Name = "igw-${var.stack_name}",
		Project = "${var.stack_name}",
		Team = "${var.team_name}"
		Environment = "Prod"
	}	
}


resource "aws_route_table" "rt-stack" {
	  vpc_id = "${var.vpc_id}"
	  route {
		cidr_block = "${var.rt_cidr}"
		gateway_id = "${var.igw_id}"
	}	

    lifecycle {
        create_before_destroy = true
    }

	tags {
		Name = "rt-${var.stack_name}"
		Project = "${var.stack_name}"
		Team = "${var.team_name}"
		Environment = "Prod"
	}
}
*/



resource "aws_main_route_table_association" "assoc_table" {
	vpc_id = "${var.vpc_id}"
	route_table_id = "${var.rt_id}"

    lifecycle {
        create_before_destroy = true
    }

}

/*
resource "aws_subnet" "subnet-stack-a" {
	vpc_id = "${var.vpc_id}"
	cidr_block = "${var.snet_cidr_a}"
	availability_zone = "${var.zona_a}"
	map_public_ip_on_launch = "true"

    lifecycle {
        create_before_destroy = true
    }

	tags {
		Name = "subnet-${var.stack_name}-a"
		Project = "${var.stack_name}"
		Team = "${var.team_name}"
		Environment = "Prod"
	}	
}

resource "aws_subnet" "subnet-stack-b" {
	vpc_id = "${var.vpc_id}"
	cidr_block = "${var.snet_cidr_b}"
	availability_zone = "${var.zona_b}"
	map_public_ip_on_launch = "true"

    lifecycle {
        create_before_destroy = true
    }

	tags {
		Name = "subnet-${var.stack_name}-b"
		Project = "${var.stack_name}"
		Team = "${var.team_name}"
		Environment = "Prod"
	}	
}

resource "aws_subnet" "subnet-stack-c" {
	vpc_id = "${var.vpc_id}"
	cidr_block = "${var.snet_cidr_c}"
	availability_zone = "${var.zona_c}"
	map_public_ip_on_launch = "true"

    lifecycle {
        create_before_destroy = true
    }

	tags {
		Name = "subnet-${var.stack_name}-c"
		Project = "${var.stack_name}"
		Team = "${var.team_name}"
		Environment = "Prod"
	}	
}
*/



resource "aws_route_table_association" "zona_a" {
	subnet_id = "${var.snet_id_a}"
	route_table_id = "${var.rt_id}"

    lifecycle {
        create_before_destroy = true
    }

}

resource "aws_route_table_association" "zona_b" {
	subnet_id = "${var.snet_id_b}"
	route_table_id = "${var.rt_id}"

    lifecycle {
        create_before_destroy = "true"
    }

}

/*
resource "aws_route_table_association" "zona_c" {
	subnet_id = "${var.snet_id_c}"
	route_table_id = "${var.rt_id}"

    lifecycle {
        create_before_destroy = "true"
    }

}

*/

resource "aws_elb" "elb-stack-layer" {
	#name = "elb-${var.stack_name}-${var.layer_name}"
	name = "${var.elb_name}"
  security_groups = ["${aws_security_group.sg_stack-layer.id}", "${aws_security_group.sg_stack-layer-elb.id}"] 
	subnets = ["${var.snet_id_a}", "${var.snet_id_b}", "${var.snet_id_c}"]
	#availability_zones = ["${var.zona_c}"]
	internal = "true"

#  access_logs {
#    bucket = "infra"
#    interval = 60
#  }

listener {
	instance_port = 22
	instance_protocol = "tcp"
	lb_port = 80
	lb_protocol = "tcp"
}

health_check {
	healthy_threshold = 3
	unhealthy_threshold = 2
	timeout = 5
	target = "TCP:22"
	interval = 10
}

cross_zone_load_balancing = true
idle_timeout = 300
connection_draining = true
connection_draining_timeout = 300

    lifecycle {
        create_before_destroy = true
    }

tags {
	Name = "elb-${var.stack_name}-${var.layer_name}"
	Project = "${var.stack_name}"
	Team = "${var.team_name}"
	Environment = "Prod"
  }   
}
