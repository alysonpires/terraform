# Security group das instancias/layer

resource "aws_security_group" "sg_stack-layer" {
  name        = "sg_${var.stack_name}-${var.layer_name}"
  description = "sg_${var.stack_name}-${var.layer_name} - INFRA-4257"
  vpc_id      = "${var.vpc_id}"
  revoke_rules_on_delete = "true"
  
  /*
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["201.76.168.107/32"]
    description = "Bemobi"
  }



  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = ["${aws_security_group.sg_stack-layer-elb.id}"]
    description     = "sg_${var.stack_name}-${var.layer_name}-elb"
  }
*/


  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["201.76.168.107/32"]
    description = "Bemobi"
  }
  
	egress {
   	from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]

    #prefix_list_ids = ["pl-12c4e678"]
  }

    lifecycle {
        create_before_destroy = true
    }


  tags {
    Name        = "sg_${var.stack_name}-${var.layer_name}"
    Project     = "${var.stack_name}"
    Team        = "${var.team_name}"
    Environment = "Prod"
  }
}

resource "aws_security_group" "sg_stack-layer-elb" {
  name        = "sg_${var.stack_name}-${var.layer_name}-elb"
  description = "sg_${var.stack_name}-${var.layer_name}-elb - INFRA-4257"
  vpc_id      = "${var.vpc_id}"
  revoke_rules_on_delete = "true"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    security_groups = ["${aws_security_group.sg_stack-layer.id}"]
    description = "sg_${var.stack_name}-${var.layer_name}"
  }


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    #prefix_list_ids = ["pl-12c4e678"]
  }

  tags {
    Name        = "sg_${var.stack_name}-${var.layer_name}-elb"
    Project     = "${var.stack_name}"
    Team        = "${var.team_name}"
    Environment = "Prod"
  }
}

# Recurso para gerenciamento do security group default que é criado junto com a VPC
resource "aws_default_security_group" "default" {
  vpc_id = "${var.vpc_id}"

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["201.76.168.107/32"]
    description = "Bemobi"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]

    #prefix_list_ids = ["pl-12c4e678"]
  }
}
