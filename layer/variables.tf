# Variaveis para REGIÃO

variable "aws_regiao" {
  description = "Region for the VPC"
  default = "us-east-1"
}
 


# Variáveis para VPC
variable "vpc_cidr" {
  description = "CIDR da VPC"
  default = "10.100.39.0/24"
}

variable "vpc_name"{
  description = "VPC da stack..."
  default = "vpc"
}

variable "vpc_id" {
  default = "vpc-23bbfb5a"
}



# Variáveis para subnets
variable "snet_cidr_a" {
  description = "CIDR da subnet"
  default = "10.100.39.64/26"
}
 
variable "snet_cidr_b" {
  description = "CIDR da subnet"
  default = "10.100.39.128/26"
}

variable "snet_cidr_c" {
  description = "CIDR da subnet"
  default = "10.100.39.192/26"
}

variable "snet_cidr_d" {
  description = "CIDR da subnet"
  default = "10.100.39.192/26"
}

variable "snet_cidr_e" {
  description = "CIDR da subnet"
  default = "10.100.5.128/26"
}

variable "snet_cidr_f" {
  description = "CIDR da subnet"
  default = "10.100.6.128/26"
}

variable "snet_id_a" {
  default = "subnet-25f29b7f"
}

variable "snet_id_b" {
  default = "subnet-e6867b82"
}

variable "snet_id_c" {
  default = "subnet-0297e62e"
}

variable "snet_id_d" {
  default = "subnet-e4745eac"
}

variable "snet_id_e" {
  default = "xxx"
}

variable "snet_id_f" {
  default = "xxx"
}



# Variáveis para Route table
variable "rt_cidr" {
  description = "Route table - Stack Infra"
  default = "0.0.0.0/0"
}

variable "rt" {
  default = "xxx"
}

variable "rt_nat" {
  default = "xxx"
}

variable "rt_id" {
	default = "rtb-bd809ec5"
}



# Variáveis para as instâncias 
variable "ami" {
  description = "Amazon Linux AMI"
  default = "Amazon Linux 2017.03"
}
 
variable "key_path" {
  description = "SSH Public Key path"
  default = "/home/core/.ssh/id_rsa.pub"
}

variable "instance_type" {
	default = "t2.micro"
}



# Variáveis para Zonas de disponibilidade da subnet
variable "zona_a" {
  default = "us-east-1a"
}

variable "zona_b" {
  default = "us-east-1b"
}

variable "zona_c" {
  default = "us-east-1c"
}

variable "zona_d" {
  default = "us-east-1d"
}

variable "zona_e" {
  default = "us-east-1e"
}


variable "zona_f" {
  default = "us-east-1f"
}



# Variaveis para security group
variable "sg-layer" {
  description = "Security Group da ${var.layer_name} - Stack ${var.stack_name}"
  default = "201.76.168.107/32"
}

variable "sg_elb" {
  description = "Security Group do elb - ${var.layer_name} - Stack ${var.stack_name}"
  default = "0.0.0.0/0"
}



# Variavel para internet gateway
variable "igw_name" {
default = "igw"
}

variable "igw_id" {
  default = "igw-86b79ce0"
}



# Variáveis para stack e layer

variable "stack_name" {
  default = "oi_plataformas"
}
variable "stack_id" {
  default = "1ed65a06-5af6-4f44-94d2-13957b967be6"
}

variable "layer_name" {
  default = "offspring-rewards"
}

variable "deploy_default" {
  default = "deploy::default"
}

variable "team_name" {
  default = "Plataformas"
}

variable "elb_name" {
  default = "elb-oi-plat-offspring-teste"
}


# Variáveis para RDS

variable "dbuser" {
	default = "root"
}

variable "dbpassword" {
	default = "123qweBemobi"
}

variable "instance_class_rds" {
	default = "db.t2.micro"
}

variable "engine" {
	default = "mysql"
}

variable "engine_version_rds" {
	default = "5.7.21"
}

variable "mydb" {
	default = "mydb"
}

variable "public_accesible" {
	default = "true"
}

variable "backup_rds" {
	default = "04:00-05:00"
}

variable "janela_manutencao" {
	default = "sat:03:00-sat:04:00"
}

variable "role_cloudwatch" {
	default = "arn:aws:iam::aws:policy/service-role/AmazonRDSEnhancedMonitoringRole"
}

variable "family" {
	default = "mysql5.7"
}



# Variáveis para elasticache - Redis/Memcached

variable "engine_redis" {
	default = "redis"
}

variable "engine_version_redis" {
	default = "3.2.10"
}

variable "node_type_redis" {
	default = "cache.m3.medium"
}

variable "parameter_group_name" {
	default = "default.redis3.2"
}

variable "port_redis" {
	default = "6379"
}

variable "notification_topic_arn" {
	default = "arn:aws:sns:us-east-1:012345678999:my_sns_topic"
}



# Teste de array

variable "tags" {
  default = {
	"Name" = "Teste"
	"Project" = "Agora vai"
	"Team" = "Sera"
	"Environment" = "Prod"
  }
}


