# Criando as instancias no opsworks
resource "aws_opsworks_instance" "instance1" {
  	stack_id = "${var.stack_id}"
  	layer_ids = ["${aws_opsworks_custom_layer.layer.id}",]

	  instance_type = "${var.instance_type}"
 	  os            = "${var.ami}"
  	state         = "stopped"
  	hostname      = "${var.layer_name}1"
  	#availability_zone = "${var.zona_a}"
  	delete_ebs    = "true"
  	delete_eip    = "true"
	# ssh_key_name  = "alyson"
	# elastic_ip    = "true"
	# public_dns    = "true"
	# public_ip     = "false"
	# ebs_optimized = "true" # Não está disponível para instancia ${var.instance_type}

    ebs_block_device {
        device_name = "/dev/xvda"
#        snapshot_id = "snap-xxxxxxxx"
        volume_size = 40
    }

    lifecycle {
        create_before_destroy = true
    }


}



/*
resource "aws_opsworks_instance" "instance2" {
  stack_id = "${var.stack_id}"
  layer_ids = ["${aws_opsworks_custom_layer.layer.id}",]

	instance_type = "${var.instance_type}"
 	os            = "${var.ami}"
  	state         = "stopped"
  	hostname      = "${var.layer_name}2"
  	availability_zone = "${var.zona_b}"
  	subnet_id     = "${aws_subnet.subnet-stack-b.id}"
  	delete_ebs    = "true"
  	delete_eip    = "true"
	# ssh_key_name  = "alyson"
	# elastic_ip    = "true"
	# public_dns    = "true"
	# public_ip     = "false"
	# ebs_optimized = "true" # Não está disponível para instancia ${var.instance_type}

    ebs_block_device {
        device_name = "/dev/xvda"
        #snapshot_id = "snap-xxxxxxxx"
        volume_size = 40
    }

    lifecycle {
        create_before_destroy = true
    }

}

resource "aws_opsworks_instance" "instance3" {
  	stack_id = "${var.stack_id}"
  	layer_ids = ["${aws_opsworks_custom_layer.layer.id}",]

	instance_type = "${var.instance_type}"
 	os            = "${var.ami}"
  	state         = "stopped"
  	hostname      = "${var.layer_name}3"
  	availability_zone = "${var.zona_c}"
  	subnet_id     = "${aws_subnet.subnet-stack-c.id}"
  	delete_ebs    = "true"
  	delete_eip    = "true"
	# ssh_key_name  = "alyson"
	# elastic_ip    = "true"
	# public_dns    = "true"
	# public_ip     = "true"
	# ebs_optimized = "true" # Não está disponível para instancia ${var.instance_type}

    ebs_block_device {
        device_name = "/dev/xvda"
        #snapshot_id = "snap-xxxxxxxx"
        volume_size = 40

    }

    lifecycle {
        create_before_destroy = true
    }

  root_block_device {
    volume_type           = "gp2"
    volume_size           = "8"
    delete_on_termination = true
  }
}
*/



/*
# Criando um EBS com 40GB - Apenas teste
resource "aws_ebs_volume" "ebs_volume" {
    availability_zone = "${var.zona_a}"
    size = 40
    tags {
        Name = "HelloWorld"
        Project = "${var.stack_name}"
        Team = "${var.team_name}"
        Environment = "Dev"
    }
}

*/
