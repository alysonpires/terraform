resource "aws_security_group" "rds-stack-layer" {
  name = "sg_rds-${var.stack_name}-${var.layer_name}"
  description = "sg_rds-${var.stack_name}-${var.layer_name}"
  vpc_id = "${aws_vpc.vpc-stack.id}"
  #revoke_rules_on_delete = "true"

	ingress {
		from_port = 3306
		to_port = 3306
		protocol = "tcp"
		security_groups = ["${aws_security_group.sg_stack-layer.id}"]
		description = "sg_${var.stack_name}-${var.layer_name}"
}

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    #prefix_list_ids = ["pl-12c4e678"]
  }

    lifecycle {
        create_before_destroy = true
    }


	tags {
		Name = "sg_rds-${var.stack_name}-${var.layer_name}"
		Project = "${var.stack_name}"
		Team = "${var.team_name}"
		Environment = "Prod"
       
	 }
}



resource "aws_db_subnet_group" "rds_snet_group" {
	name = "rds_snet_group"
	subnet_ids = ["${aws_subnet.subnet-stack-a.id}", "${aws_subnet.subnet-stack-b.id}", "${aws_subnet.subnet-stack-c.id}"]
	description = "rds_snet_group-${var.stack_name}-${var.layer_name}"

    lifecycle {
        create_before_destroy = true
    }

	tags {
		Name = "rds-${var.stack_name}-subnet-group"
		Project = "${var.stack_name}"
		Team = "${var.team_name}"
		Environment = "Prod"
    
	}
}



resource "aws_db_instance" "rds" {
  allocated_storage    = 10
  storage_type         = "gp2"
  engine               = "${var.engine}"
  engine_version       = "${var.engine_version_rds}"
  instance_class       = "${var.instance_class_rds}"
  name                 = "${var.mydb}"
  username             = "${var.dbuser}"
  password             = "${var.dbpassword}"
  publicly_accessible  = "${var.public_accesible}"
  parameter_group_name = "${aws_db_parameter_group.rds-group.id}"
  allow_major_version_upgrade = "false" # Vale a pena debater essa opção

# Opção perigosa, pois deleta o banco e cria novamente do zero.
	#storage_encrypted = "false"
	apply_immediately = "true"
	#auto_minor_version_upgrade = "false"
 	#availability_zone = "${var.zona_b}"
	backup_window = "${var.backup_rds}"
	copy_tags_to_snapshot = "1"
	final_snapshot_identifier = "rds-${var.stack_name}-${var.layer_name}"
	identifier = "rds-${var.stack_name}-${var.layer_name}"
	storage_type = "gp2"
	backup_retention_period = 1
	maintenance_window = "${var.janela_manutencao}"
	multi_az = "true"
	vpc_security_group_ids = ["${aws_security_group.sg_stack-layer.id}",]
	db_subnet_group_name = "${aws_db_subnet_group.rds_snet_group.id}"
	#db_subnet_group_name = "rds_snet_group"
	#port = "8000" # Caso queira que o banco trabalhe em outra porta
	#monitoring_interval = "15"
	#monitoring_role_arn = "${var.role_cloudwatch}"
	skip_final_snapshot = "true"
	#snapshot_identifier = "rds:rds-infra-devops-replica-2018-03-30-20-16"

    lifecycle {
        create_before_destroy = true
    }

  timeouts {
    create = "60m"
    delete = "2h"

	}

	tags {
        Name = "rds-${var.stack_name}-${var.layer_name}"
        Team = "${var.team_name}"
        Project = "${var.stack_name}"
        Environment = "Prod"
 
	}

}



resource "aws_db_instance" "rds-replica" {
  	allocated_storage    = 10
  	engine               = "${var.engine}"
  	engine_version       = "${var.engine_version_rds}"
  	instance_class       = "${var.instance_class_rds}"
  	#identifier           = "rds-${var.stack_name}-${var.layer_name}-replica"
  	name                 = "${var.mydb}"
  	username             = "${var.dbuser}"
  	password             = "${var.dbpassword}"
  	vpc_security_group_ids = ["${aws_security_group.sg_stack-layer.id}"]
  	parameter_group_name = "${aws_db_parameter_group.rds-group.id}"
  	multi_az = "false"
  	storage_type = "gp2"
  	maintenance_window = "sat:05:00-sat:06:00"
	backup_window = "${var.backup_rds}"
  	#backup_retention_period = 1
  	replicate_source_db = "${aws_db_instance.rds.id}"
	#monitoring_interval = "15"
	#monitoring_role_arn = "${var.role_cloudwatch}"
	skip_final_snapshot = "true"
	#final_snapshot_identifier = "rds-${var.stack_name}-${var.layer_name}-replica"
  	identifier           = "rds-${var.stack_name}-${var.layer_name}-replica"
	#snapshot_identifier = "rds:rds-infra-devops-replica-2018-03-30-20-16"

    lifecycle {
        create_before_destroy = true
    }
 
  timeouts {
    create = "60m"
    delete = "2h"
  } 

	tags {
        Name = "rds-${var.stack_name}-${var.layer_name}-replica"
        Team = "${var.team_name}"
        Project = "${var.stack_name}"
        Environment = "Prod"    
	}
}



resource "aws_db_parameter_group" "rds-group" {
    name = "rds-group-${var.stack_name}"
	description = "rds-group-${var.stack_name}"
    family = "${var.family}"

  parameter {
    name         = "rds.force_ssl"
    value        = "1"
    apply_method = "pending-reboot"
  }

    parameter {
      name = "event_scheduler"
      value = "ON"
      apply_method = "pending-reboot"
    }

    parameter {
      name = "general_log"
      value = "0"
      apply_method = "pending-reboot"
    }

    parameter {
      name = "interactive_timeout"
      value = "3600"
      apply_method = "pending-reboot"
    }

    parameter {
      name = "log_bin_trust_function_creators"
      value = "1"
      apply_method = "pending-reboot"
    }

    parameter {
      name = "long_query_time"
      value = "60"
      apply_method = "pending-reboot"
    }

    parameter {
      name = "max_allowed_packet"
      value = "1073741824"
      apply_method = "pending-reboot"
    }

    parameter {
      name = "max_connections"
      value = "550"
      apply_method = "pending-reboot"
    }

    parameter {
      name = "performance_schema"
      value = "1"
      apply_method = "pending-reboot"
    }

    parameter {
      name = "slow_query_log"
      value = "1"
      apply_method = "pending-reboot"
    }

    parameter {
      name = "time_zone"
      value = "UTC"
      apply_method = "pending-reboot"
    }

    parameter {
    name  = "character_set_server"
    value = "utf8"
    }

    parameter {
    name  = "character_set_client"
    value = "utf8"
    }

    parameter {
      name = "wait_timeout"
      value = "3600"
      apply_method = "pending-reboot"
    }

    lifecycle {
        create_before_destroy = true
    }

}

